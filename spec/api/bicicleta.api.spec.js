var Bicicleta = require('../../models/bicicleta')
var request =require('request')
var server = require('../../bin/www')

beforeEach(() => {
  Bicicleta.allBicis = [];
});

describe('Bicicleta API', ()=> {

  //   afterEach(function(done) {
  //     Bicicleta.deleteMany({}, function(err, success) {
  //         if (err) console.log(err);
  //         done();
  //     });
  // });

  describe('GET BICICLETAS /', () => {
    it('STATUS 200', (done) => {
      expect(Bicicleta.allBicis.length).toBe(0);
      
      request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
          expect(response.statusCode).toBe(200);
          done();
      });

      
    });
  });


    // describe('GET Bicicleta /', ()=> {
    //   it('Status 200 ', (done)=> {
    //     // expect(Bicicleta.allBicis.length).toBe(0);

    //     // var a = new Bicicleta(1,'rojo','urbana',[51.5, -0.09]);
    //     // Bicicleta.add(a);

    //     request.get('http://localhost:3000/api/bicicletas',(err,res,body)=>{
    //       expect(res.statusCode).toBe(200);
    //       done();
    //     });
    //   });
    // });

    describe('POST Bicicleta /create', ()=> {
      it('Status 200 ', (done)=> {
        var headers = { 'content-type' : 'application/json' };
        var a = '{"code":10,"color":"rojo","modelo":"urbana","lat":51.5, "lng":-0.09}'
        
        request.post({
          headers: headers,
          url: 'http://localhost:3000/api/bicicletas/create',
          // body: JSON.stringify(a)
          body: a
        }, function(err,res,body){
          console.log("Body: "+ body)
          var bici = JSON.parse(body).bicicleta
          expect(res.statusCode).toBe(200)
          expect(bici.color).toBe('rojo');
          done();
        })        
          
      })    
    })


})
