
var mongoose = require('mongoose');

var Bicicletas = require('../../models/bicicleta')

describe('Bicicleta API', () => {

  beforeAll(function (done) {

      mongoose.connection.close().then(() => {
          var mongoDB = 'mongodb://localhost/testdb';
          mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

          mongoose.set('useCreateIndex', true);
          
          var db = mongoose.connection;
          db.on('error', console.error.bind(console, 'MongoDB connection error: '));
          db.once('open', function () {
              console.log('We are connected to test database!');
              done();

          });
      });
  });

  afterEach(function (done) {
      Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          done();
      });
  });
});


// beforeEach(()=>{
//   Bicicletas.allBicis = []
// })

// describe('Bicicletas.allBicis', ()=> {
//   it('comienza vacio', ()=> {
//     expect(Bicicletas.allBicis.length).toBe(0)
//   })
// })

// describe('Bicicletas.add', ()=> {
//   it('agregar una bici', ()=> {
//     expect(Bicicletas.allBicis.length).toBe(0)

//     var a = new Bicicletas(1,'rojo','urbana',[51.5, -0.09])
//     Bicicletas.add(a)

//     expect(Bicicletas.allBicis.length).toBe(1)
//     expect(Bicicletas.allBicis[0]).toBe(a)    
//   })
// })


// describe('Bicicletas.findById', ()=> {
//   it('devolver id 1', ()=> {
//     expect(Bicicletas.allBicis.length).toBe(0)

//     var a = new Bicicletas(1,'rojo','urbana',[51.5, -0.09])
//     var b = new Bicicletas(2,'blanca','urbana',[51.51, -0.08])

//     Bicicletas.add(a);
//     Bicicletas.add(b);

//     var targetBici = Bicicletas.findById(1)

//     expect(targetBici.id).toBe(1)    
//     expect(targetBici.color).toBe(a.color)    
//     expect(targetBici.modelo).toBe(a.modelo)    

//   })
// })

// describe('Bicicletas.removeById', ()=> {
//   it('Test removeById', ()=> {
//     expect(Bicicletas.allBicis.length).toBe(0)

//     var b = new Bicicletas(2,'blanca','urbana',[51.51, -0.08])
//     Bicicletas.add(b);

//     Bicicletas.removeById(2)


//     expect(Bicicletas.allBicis.length).toBe(0)
//   })
// })