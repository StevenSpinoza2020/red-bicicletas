var mongoose = require('mongoose');
var Reserva = require('./reserva');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var Token = require('../models/token');
var mailer = require('../mailer/mailer');
var uniqueValidator = require('mongoose-unique-validator');
const saltRounds = 10;

var Schema = mongoose.Schema;

const validateEmail = function (email) {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
};

var usuarioSchema = new Schema({
    // nombre: String,
    nombre: {
        type: String,
        trim: true,
        required: [true,'Nombre obligatorio']
    },
    email: {
        type: String,
        unique: true,
        trim: true,
        required: [true, 'Email obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'Ingrese email valido'],
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password: {
        type: String,
        required: [true, 'Password obligatorio'],
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
})


usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.plugin(uniqueValidator, {
    message: 'El {PATH} ya existe con otro usuario.'
});


usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err) {
        if (err) { return cb(err); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link:\n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return cb(err); }
            console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
        });

        cb(null);
    });
};


usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb){
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId, 
        desde: desde, 
        hasta: hasta
    });
    
    reserva.save(cb);
}


usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex')
    });
    const email_destination = this.email;
    
    token.save(function(err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en el siguiente link: \n\n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) {
                return console.log(err.message);
            }

            console.log('Se ha enviado un email de bienvenida a ' + email_destination + '.');
        });
    });
};


usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreateByGoogle(condition, callback) {

    const self = this;
    console.log(condition);
    self.findOne( {
        $or: [
            { 'googleId': condition.id }, {'email': condition.emails[0].value}
    ]}, (err, result) => {
        if (result) {
            callback(err, result)
         }
        else {
            console.log('--------- CONDITION ---------');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0]?condition.emails[0].value : null;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = condition._json.etag || 'oauth';
            console.log('----------- VALUES ----------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err)  console.log(err); 
                return callback(err, result)
            })
        }
    })
};


usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreateByFacebook(condition, callback) {

    const self = this;
    console.log(condition);
    self.findOne( {
        $or: [
            { 'facebookId': condition.id }, {'email': condition.emails[0].value}
    ]}, (err, result) => {
        if (result) {
            callback(err, result)
         }
        else {
            console.log('--------- CONDITION ---------');
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('----------- VALUES ----------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err)  console.log(err); 
                return callback(err, result)
            })
        }
    })
};


module.exports = mongoose.model('Usuario', usuarioSchema);