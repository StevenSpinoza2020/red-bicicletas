var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: { type: [Number], index: {type: '2dsphere',sparse:true}}
})

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
  return new this({
      code: code,
      color: color,
      modelo: modelo,
      ubicacion: ubicacion
  });
};

bicicletaSchema.methods.toString = function(){
  return 'code: ' + this.code + "| color: " + this.color
}

bicicletaSchema.statics.allBicis = function(cb){
  return this.find({},cb)
}

bicicletaSchema.statics.add = function (aBici, cb) {
  return this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
}

bicicletaSchema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
} 

module.exports = mongoose.model('Bicicleta', bicicletaSchema)




// var Bicicleta = function (id, color, modelo, ubicacion){
//   this.id = id;
//   this.color = color;
//   this.modelo = modelo;
//   this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function(){
//   return 'id: ' + this.id + "| color: " + this.color
// }

// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici){
//   Bicicleta.allBicis.push(aBici)
// }

// Bicicleta.findById = function(biciId){
//   // console.log("biciID: "+biciId)
//   // console.log("Bicicleta.allBicis: "+Bicicleta.allBicis[0].id == biciId)
//   var aBici = Bicicleta.allBicis.find(b=>b.id ==biciId)
//   if(aBici) return aBici
//   else throw new Error(`No existe esa Bici con id: ${aBici}`)
// }

// Bicicleta.removeById = function(biciId){
//   // var aBici = Bicicleta.findById(biciId)
//   for(i=0; i<Bicicleta.allBicis.length; i++){
//     if(Bicicleta.allBicis[i].id == biciId){
//       Bicicleta.allBicis.splice(i,1)
//       break;
//     }
//   }
// }

// var a = new Bicicleta(1,'rojo','urbana',[51.5, -0.09])
// var b = new Bicicleta(2,'blanca','urbana',[51.51, -0.08])

// Bicicleta.add(a);
// Bicicleta.add(b);

// module.exports = Bicicleta