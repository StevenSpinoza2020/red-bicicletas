var map = L.map('main_map').setView([51.505, -0.09], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([51.5, -0.09]).addTo(map)
// L.marker([51.51, -0.08]).addTo(map)
// L.marker([51.5, -0.06]).addTo(map)

$.ajax({
  dataType:'json',
  url:'api/bicicletas',
  success: function(result){
    // console.log(result)
    result.bicicletas.forEach(function(bici) {
      console.log(bici.ubicacion)
      L.marker(bici.ubicacion,{title:bici.id}).addTo(map)
    });
  }
})
