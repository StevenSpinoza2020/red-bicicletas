const Bicicleta = require('../../models/bicicleta')

exports.bicicleta_list = function(req,res){
  res.status(200).json({
    bicicletas: Bicicleta.allBicis
  })
}

exports.bicicleta_create = function(req,res){

  const body = JSON.stringify(req.body)
  console.log("bicicleta_create body"+ body)
  

  // var bici = new Bicicleta(body.code, body.color, body.modelo,[body.lat, body.lng])
  var bici = new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng] });
  console.log("bicicleta_create bici: "+ bici.code)
  Bicicleta.add(bici)
  
  res.status(200).json({
    bicicleta: bici
  })
}

exports.bicicleta_delete = function(req,res){
  
  Bicicleta.removeById(req.body.id)
  
  res.status(204).send()
}

