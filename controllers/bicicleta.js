var Bicicleta = require('../models/bicicleta')

exports.bicicleta_list = function(req,res){
  res.render('bicicletas/index', {bicis:Bicicleta.allBicis})
}

exports.bicicleta_create_get = function(req,res){
  res.render('bicicletas/create')
}

exports.bicicleta_create_post = function(req,res){
  
  let body = req.body

  var bici = new Bicicleta(body.id, body.color, body.modelo)
  bici.ubicacion = [body.lat, body.lng]
  // Bicicleta.add(bici)
  Bicicleta.add(bici, function (err, newBici) {
    if (err) console.log('ERROR Bicicleta add: '+err);
  });

  res.redirect('/bicicletas')
}

exports.bicicleta_update_get = function(req,res){
  
  var bici = Bicicleta.findById(req.params.id)

  res.render('bicicletas/update',{bici})
}

exports.bicicleta_update_post = function(req,res){
  
  let body = req.body
  
  var bici = Bicicleta.findById(req.params.id)
  bici.id = body.id
  bici.color = body.color
  bici.modelo = body.modelo
  bici.ubicacion = body.ubicacion
  bici.ubicacion = [body.lat, body.lng]

  res.redirect('/bicicletas')
}

exports.bicicleta_delete_post = function(req,res){
  Bicicleta.removeById(req.body.id)

  res.redirect('/bicicletas')
}